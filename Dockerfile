FROM python:3

RUN set -ex && \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
        make build-essential libssl-dev zlib1g-dev libbz2-dev \
        libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
        xz-utils tk-dev libffi-dev liblzma-dev python3-openssl git unzip default-jre

WORKDIR /multivac

COPY requirements.txt /multivac/requirements.txt
COPY config.json /root/.config/Red-DiscordBot/config.json

RUN set -ex && \
    pip install --upgrade -r requirements.txt