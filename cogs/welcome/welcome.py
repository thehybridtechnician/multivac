from redbot.core import commands
import discord
import re

import asyncio


class WelcomeCog(commands.Cog):
    """Welcome Cog for `Multivac`"""

    @commands.command(name="AGREE")
    async def landing_page_agreement(self, ctx):

        def not_pinned(message):
            return message.pinned == False

        if ctx.guild.id == 567898828544409600 and ctx.channel.id == 605034229939830796:
            if not ctx.author.bot:
                user = ctx.author
                channel = ctx.channel
                nickname = re.sub(r'^(.*AGREE\s*)', "", ctx.message.content, flags=re.IGNORECASE)
                if nickname:
                    await ctx.send(
                        f"Thank you {user.name}.  Welcome to __**Secular North**__."
                        "You will now be moved to General Chat and given the `member` role\n"
                        "Take a look at the Channel Panel on the << Left Side.  Once you have the member role,"
                        "you will see a lot more channels.  I would recommend checking out of checking out the"
                        "#about channel for a review of server rules.  The #general-chat in the General"
                        "Category is a great place to introduce yourself.  Thank you and enjoy yourself on __**Secular North**__",
                        delete_after=10
                    )
                    
                    await user.edit(nick=nickname, reason="Initial Server Join.")
                    role = ctx.guild.get_role(600174635186913290)
                    await user.add_roles(role)

                    embed = discord.Embed(title=f"{user.nick}", color=0x009933)
                    embed.add_field(name=f"Join Date", value=f" {user.joined_at.strftime('%B %d, %Y')}", inline=True)
                    embed.set_author(name="Member Joined", icon_url=user.avatar_url)

                    general_channel = ctx.guild.get_channel(567898828544409602)
                    about_channel = ctx.guild.get_channel(567902070842195978)
                    request_channel = ctx.guild.get_channel(604814966708633722)
                    activism_channel = ctx.guild.get_channel(601054718424252427)
                    share_channel = ctx.guild.get_channel(600126090455023617)

                    await general_channel.send(embed=embed)

                    await general_channel.send(
                        f"Welcome {user.mention} to __**Secular North**__.  We are glad you joined us.\n"
                        f"{about_channel.mention} contains server information and rules.  {general_channel.mention} handles basic communication. "
                        f"To request access to the different organization like HumanistsMN, check out {request_channel.mention}. "
                        f"Also, discuss getting Active in {activism_channel.mention} or if you are a *Content Creator*, share your content in {share_channel.mention}"
                        "Anyways, say hi to all on the server and let's work together to build a more Secular North."
                    )

                    await asyncio.sleep(10)
                    await channel.purge(check=not_pinned)
                else:
                    await ctx.send(
                        "Uh Oh...  Looks like the command didn't work.\n"
                        "Please make sure to include your name.  If you have questions about any of the rules of "
                        "__**Secular North**__, don't hesitate to reach out to Justin Bovee."
                    )

    # @commands.Cog.listener()
    # async def on_member_join(self, member):
    #     if ctx.guild.id == 567898828544409600 and ctx.channel.id == 605034229939830796:

    #         if not member.bot:
    #             embed = discord.Embed(title=f"{member.name}#{member.discriminator}", color=0x009933)
    #             embed.add_field(name=f"Creation Date", value=f" {member.created_at.strftime('%B %d, %Y')}", inline=True)
    #             embed.add_field(name=f"Join Date", value=f" {member.joined_at.strftime('%B %d, %Y')}", inline=True)
    #             embed.set_author(name="Member Joined", icon_url=member.avatar_url)
    #             try:
    #                 channel = member.guild.get_channel(567898828544409602)
    #                 await channel.send(embed=embed)
    #             except:
    #                 print(f"Unable to send welcome message to {member.name}")
    #             try:
    #                 role = member.guild.get_role(600174635186913290)
    #                 print(role)
    #                 await member.add_roles(role)
    #             except Exception as e:
    #                 print(f"Unable set roles for {member.name} because {e}")
