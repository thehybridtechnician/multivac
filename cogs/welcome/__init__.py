from redbot.core.bot import Red
from .welcome import WelcomeCog

def setup(bot: Red):
    welcome = WelcomeCog(bot)
    bot.add_cog(welcome)